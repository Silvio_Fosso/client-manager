﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DGVPrinterHelper;
namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public String Codice;
        public int selezionato;
        public BindingList<Azienda> azienda;
        
        public Form2()
        {
            InitializeComponent();
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
       (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private async void button1_ClickAsync(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != ""&& textBox3.Text != ""&& textBox4.Text != "")
            {
                await Request.shared.sendRisorse(int.Parse(Codice), textBox4.Text, textBox1.Text, textBox2.Text,textBox3.Text);
                MessageBox.Show("Record inserito con successo");
                textBox1.Text = "0";
                textBox2.Text = "0";
                textBox3.Text = "";
                var req = await Request.shared.getazienda(Codice);
                azienda = req;
                dataGridView1.DataSource = req;
                avereclient(azienda);
            }
            else {
                MessageBox.Show("Inserire tutti i campi");
            }
        }

        public void avereclient(BindingList<Azienda> cliente) {
            float somma = 0;
            float differenza = 0;
            foreach (Azienda cli in cliente) {
                somma += cli.Avere;
                differenza += cli.Dare;
            }
            label4.Text = "Saldo Totale : "+(somma - differenza).ToString("c2");
        }

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
       (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void cancellaRigaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
           
        }

        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
           
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            string id = dataGridView1.Rows[e.Row.Index].Cells["Id"].Value.ToString();
            Request.shared.deleterow(id);
            //Console.WriteLine(azienda.Count);

          
        }

        private void dataGridView1_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            string id = dataGridView1.Rows[e.RowIndex].Cells["Id"].Value.ToString();
            string name = dataGridView1.Columns[e.ColumnIndex].Name;
            
            string value = dataGridView1.Rows[e.RowIndex].Cells[name].Value.ToString();
            Console.WriteLine(value);
            if (name == "Dare")
            {
                azienda[e.RowIndex].Dare = float.Parse(value);
                avereclient(azienda);
            }
            else if(name == "Avere"){
                azienda[e.RowIndex].Avere = float.Parse(value);
                avereclient(azienda);
            }
            Request.shared.updaterow(id,value,name);
        }

        private void dataGridView1_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            avereclient(azienda);
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void stampaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stampa();

        }

        void stampa() {

            DGVPrinter printer = new DGVPrinter();
            printer.Title = string.Format("Dr.Giuseppe Fosso", printer.TitleFont = new Font("Arial", 24, FontStyle.Bold | FontStyle.Underline), printer);
            printer.SubTitle = "Azienda : " + label1.Text;
            printer.SubTitleFormatFlags = StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
            printer.PageNumbers = true;
            printer.PageNumberInHeader = false;
            printer.PorportionalColumns = false;
            printer.Footer = label4.Text;
            printer.HeaderCellAlignment = StringAlignment.Near;
            printer.FooterSpacing = 15;
            printer.ColumnWidths.Add("Dare", 150);
            printer.ColumnWidths.Add("Avere", 150);
            printer.ColumnWidths.Add("Data", 200);
            printer.ColumnWidths.Add("Descrizione", 200);
            printer.PrintDataGridView(dataGridView1);
           
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
    (e.KeyChar != '.') &&
    (e.KeyChar != '/'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
        void design()
        {
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;

            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            button2.BackColor = Color.FromArgb(20, 25, 72);
            button2.Text = "\uD83D\uDD0D";
            DateTime dateTime = DateTime.UtcNow.Date;
            textBox4.Text = dateTime.ToString("dd/MM/yyyy");
            this.Text = label1.Text;
            design();
           
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Popup2 p2 = new Popup2();
            p2.dv = getDatatable();
            p2.dt = dataGridView1;
            p2.StartPosition = FormStartPosition.CenterScreen;
            p2.Show();
        }
        DataTable getDatatable()
        {
            DataTable dt = new DataTable();
            foreach (DataGridViewColumn col in dataGridView1.Columns)
            {
                dt.Columns.Add(col.Name);
            }

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                DataRow dRow = dt.NewRow();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    dRow[cell.ColumnIndex] = cell.Value;
                }
                dt.Rows.Add(dRow);
            }
            return dt;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}

