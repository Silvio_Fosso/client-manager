﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DGVPrinterHelper;
using System.Reflection;
using System.Net;
using System.IO;
using System.Diagnostics;
using Tulpep.NotificationWindow;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }
        DataTable dt = new DataTable();
        private async void Form1_LoadAsync(object sender, EventArgs e)
        {
            label4.Anchor = AnchorStyles.Left | AnchorStyles.Top;
            label6.Text = "\uD83D\uDD0D";
            label4.BringToFront();
            label1.BringToFront();
            string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Checkup(version);
            label3.Text = "Versione : " + version;
            var funz = await Request.shared.Getfunz();
            if (funz != "0") {
                Application.ExitThread();
            }
            var req = await Request.shared.getAziendeAsync();
           
            setupGrid(req,dataGridView1,0);
            dt = getDatatable();
            var req1 = await Request.shared.GetEvery();
            float somma = 0f;
            float differenza = 0f;
            req1.ForEach(x=> {
                somma += x.Avere;
                differenza += x.Dare;
            });
            label4.Text = "Il saldo totale è : " + (somma-differenza).ToString("c2");
            design();
            
            
        }

        void design() {
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;

            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }

        async void Checkup(string ver) {
            var req = await Request.shared.GetVersion();
            if (req == ver)
            {
               // MessageBox.Show("L'applicazione è gia aggiornata all'ultima versione");
            }
            else
            {
                PopupNotifier popup = new PopupNotifier();
                popup.TitleText = "Update";
                popup.ContentText = "E' stato trovato un aggiornamento alla versione : "+req+". \nSi consiglia di aggiornare il prima possibile";
                popup.Popup();
            }
        }
        void setupGrid(BindingList<Aziend> req,DataGridView dt, int i ) {
            if (i == 0)
            {
                dt.DataSource = req;
                dt.Columns["Codice"].Visible = false;
                dt.Columns["Nome"].HeaderText = "AZIENDE";
                dt.Columns["Nome"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Nome"].HeaderCell.Style.Font = new Font("Arial", 20F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Nome"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Nome"].DefaultCellStyle.Font = new Font("Arial", 30F, FontStyle.Bold, GraphicsUnit.Pixel);
                dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            }
            else {
                //dt.DataSource = req;
               // dt.Columns["Codice"].Visible = false;
               // dt.Columns["Nome"].HeaderText = "AZIENDE";
                dt.Columns["Dare"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Id"].Visible = false;
                dt.Columns["Dare"].HeaderCell.Style.Font = new Font("Arial", 20F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Dare"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Dare"].DefaultCellStyle.Font = new Font("Arial", 30F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Data"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Data"].HeaderCell.Style.Font = new Font("Arial", 20F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Data"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Data"].DefaultCellStyle.Font = new Font("Arial", 30F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Avere"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Avere"].HeaderCell.Style.Font = new Font("Arial", 20F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Avere"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Avere"].DefaultCellStyle.Font = new Font("Arial", 30F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Descrizione"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Descrizione"].HeaderCell.Style.Font = new Font("Arial", 20F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Descrizione"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Descrizione"].DefaultCellStyle.Font = new Font("Arial", 15F, FontStyle.Bold, GraphicsUnit.Pixel);

                dt.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            }
        }

       

        

        private async void dataGridView1_CellDoubleClickAsync(object sender, DataGridViewCellEventArgs e)
        {
            var req = await Request.shared.getazienda(dataGridView1.Rows[e.RowIndex].Cells["Codice"].Value.ToString());
            Form2 f2 = new Form2();
            f2.avereclient(req);
            f2.dataGridView1.DataSource = req;
            f2.azienda = req;
            setupGrid(null, f2.dataGridView1, 1);
            f2.label1.Text = dataGridView1.Rows[e.RowIndex].Cells["Nome"].Value.ToString();
            f2.Codice = dataGridView1.Rows[e.RowIndex].Cells["Codice"].Value.ToString();
            f2.Show();
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            string id = dataGridView1.Rows[e.Row.Index].Cells["Codice"].Value.ToString();
            Request.shared.deleAzienda(id);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private async void updateToolStripMenuItem_ClickAsync(object sender, EventArgs e)
        {
            string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Console.WriteLine(version);
            var req = await Request.shared.GetVersion();
            if (req == version)
            {
                MessageBox.Show("L'applicazione è gia aggiornata all'ultima versione");
            }
            else {
                MessageBox.Show("E' stato trovato un aggiornamento, dopo che avrai premuto ok , l'app si aggiornerà automaticamente");
                Process.Start("Update.exe");
                Application.ExitThread();
            }
        


        }

        private void fIleToolStripMenuItem_Click_1(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Popup p = new Popup();
            p.StartPosition = FormStartPosition.CenterScreen;
            p.dt = dataGridView1;
            p.Show();
            
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            
            DataView dv = new DataView(dt);
            dv.RowFilter = string.Format("Nome LIKE '%{0}%'",textBox1.Text);
            dataGridView1.DataSource = dv;
        }

        DataTable getDatatable() {
            DataTable dt = new DataTable();
            foreach (DataGridViewColumn col in dataGridView1.Columns)
            {
                dt.Columns.Add(col.Name);
            }

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                DataRow dRow = dt.NewRow();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    dRow[cell.ColumnIndex] = cell.Value;
                }
                dt.Rows.Add(dRow);
            }
            return dt;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }

    public class Request {
       public static Request shared = new Request();
        private static readonly HttpClient client = new HttpClient();
       public  async Task<BindingList<Aziend>> getAziendeAsync() {
            var responseString = await client.GetStringAsync("http://pxgamers.altervista.org/Papa/FullAziende.php");
            var SteamDetails = JsonConvert.DeserializeObject<BindingList<Aziend>>(responseString);
            return SteamDetails;
        }

        public async Task<string> Getfunz()
        {
            var responseString = await client.GetStringAsync("https://pxgamers.altervista.org/Papa/getfung.php");
            Console.WriteLine(responseString);
            return responseString;
        }

        public async Task<string> GetVersion()
        {
            var responseString = await client.GetStringAsync("http://pxgamers.altervista.org/Papa/Checkversion.php");
            Console.WriteLine(responseString);
            return responseString;
        }


        public async Task<List<Azienda>> GetEvery()
        {
            var responseString = await client.GetStringAsync("http://pxgamers.altervista.org/Papa/getevery.php");
            var SteamDetails = JsonConvert.DeserializeObject<List<Azienda>>(responseString);
            return SteamDetails;
        }



        public async Task<BindingList<Azienda>> getazienda(string cod)
        {
            var values = new Dictionary<string, string>
             {
               { "int", cod.ToString()}

            };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("http://pxgamers.altervista.org/Papa/GetAzienda.php", content);

            var responseString = await response.Content.ReadAsStringAsync();
            var SteamDetails = JsonConvert.DeserializeObject<BindingList<Azienda>>(responseString);
            return SteamDetails;
        }

        public async void sendAzienda(String Azienda)
        {
            var values = new Dictionary<string, string>
             {
               { "az", Azienda}
            
            };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("http://pxgamers.altervista.org/Papa/nuova.php", content);

            var responseString = await response.Content.ReadAsStringAsync();

        }

        public async void deleterow(String id)
        {
            var values = new Dictionary<string, string>
             {
               { "id", id}

            };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("http://pxgamers.altervista.org/Papa/DeleteRow.php", content);

            var responseString = await response.Content.ReadAsStringAsync();

        }

        public async void deleAzienda(String id)
        {
            var values = new Dictionary<string, string>
             {
               { "id", id}

            };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("http://pxgamers.altervista.org/Papa/deleteAzienda.php", content);

            var responseString = await response.Content.ReadAsStringAsync();

        }

        public async void updaterow(String id,string val, string name)
        {
            var values = new Dictionary<string, string>
             {
               { "id", id},
                {"name",name },
                {"val",val }

            };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("http://pxgamers.altervista.org/Papa/updateValue.php", content);

            var responseString = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseString);
        }


        public async Task sendRisorse(int codice,string data,string dare, string avere,string descrizione)
        {
            var c = data.Replace("/", "-");
            Console.WriteLine(c);
            var values = new Dictionary<string, string>
             {
               { "cod", codice.ToString()},
               { "data", c},
               { "dare", dare.ToString()},
               { "avere", avere.ToString()},
               { "Descrizione", descrizione}

            };
            
            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("http://pxgamers.altervista.org/Papa/nuovoCodice.php", content);

            var responseString = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseString);
        }
    }

    public class Aziend
    {
        public string Nome { get; set; }
        public int Codice { get; set; }

    }
    public class Azienda
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public float Dare { get; set; }
        public float Avere { get; set; }
        public string Descrizione { get; set; }
    }

}
