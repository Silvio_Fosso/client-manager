﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Popup2 : Form
    {
        public Popup2()
        {
            InitializeComponent();
           
        }
        public  DataGridView dt = new DataGridView();
        public  DataTable dv = new DataTable();
        Dictionary<string, bool> Buttonsrad = new Dictionary<string, bool>();
        private void Popup2_Load(object sender, EventArgs e)
        {
            label6.Text = "\uD83D\uDD0D";
            label5.BackColor = Color.FromArgb(20, 25, 72);
            setup();
        }
        void setup() {
            Buttonsrad.Clear();
            Buttonsrad.Add(Data.Name,Data.Checked);
            Buttonsrad.Add(Dare.Name, Dare.Checked);
            Buttonsrad.Add(Avere.Name, Avere.Checked);
            Buttonsrad.Add(Descrizione.Name, Descrizione.Checked);
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            DataView dc = new DataView(dv);
            string name = "";
            foreach (var dic in Buttonsrad) {
                if (dic.Value == true) {
                    name = dic.Key;
                }
            }
            Console.WriteLine(name);
            dc.RowFilter = string.Format(name+" LIKE '%{0}%'", textBox1.Text);
            dt.DataSource = dc;
          
        }

        private void Popup2_FormClosing(object sender, FormClosingEventArgs e)
        {
            dt.DataSource = dv;
        }

        private void Data_Click(object sender, EventArgs e)
        {
            setup();
        }

        private void Data_CheckedChanged(object sender, EventArgs e)
        {
           
        }
    }
}
