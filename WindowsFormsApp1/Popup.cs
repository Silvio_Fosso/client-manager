﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Popup : Form
    {
        public Popup()
        {
            InitializeComponent();
        }
        bool focus = false;
        private void Popup_Load(object sender, EventArgs e)
        {
           // this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            //this.BackColor = Color.Transparent;
        }
        public DataGridView dt = new DataGridView();

        protected override void OnPaint(PaintEventArgs e)
        {
           
                if (focus)
                {
                    textBox1.BorderStyle = BorderStyle.None;
                    Pen p = new Pen(Color.Blue);
                    Graphics g = e.Graphics;
                    int variance = 3;
                    g.DrawRectangle(p, new Rectangle(textBox1.Location.X - variance, textBox1.Location.Y - variance, textBox1.Width + variance, textBox1.Height + variance));
                   label3.ForeColor = Color.DodgerBlue;
               // textBox1.ForeColor = Color.DodgerBlue;
               
            }
                else
                {
                    textBox1.BorderStyle = BorderStyle.FixedSingle;
                   textBox1.ForeColor = Color.DarkGray;
                   label3.ForeColor = Color.DarkGray;
            }
            
            base.OnPaint(e);
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            focus = false;
            this.Refresh();
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            focus = true;
            this.Refresh();
        }

        private async void button1_ClickAsync(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                Request.shared.sendAzienda(textBox1.Text);
                MessageBox.Show("Il caricamento e' andato a buon fine");
                textBox1.Text = "";
                var req = await Request.shared.getAziendeAsync();
                setupGrid(req, dt, 0);
            }
            else
            {
                MessageBox.Show("Inserisci il nome dell'azienda per continuare");
            }
        }


        void setupGrid(BindingList<Aziend> req, DataGridView dt, int i)
        {
            if (i == 0)
            {
                dt.DataSource = req;
                dt.Columns["Codice"].Visible = false;
                dt.Columns["Nome"].HeaderText = "AZIENDE";
                dt.Columns["Nome"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Nome"].HeaderCell.Style.Font = new Font("Arial", 20F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Nome"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Nome"].DefaultCellStyle.Font = new Font("Arial", 30F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            }
            else
            {
                //dt.DataSource = req;
                // dt.Columns["Codice"].Visible = false;
                // dt.Columns["Nome"].HeaderText = "AZIENDE";
                dt.Columns["Dare"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Id"].Visible = false;
                dt.Columns["Dare"].HeaderCell.Style.Font = new Font("Arial", 20F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Dare"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Dare"].DefaultCellStyle.Font = new Font("Arial", 30F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Data"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Data"].HeaderCell.Style.Font = new Font("Arial", 20F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Data"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Data"].DefaultCellStyle.Font = new Font("Arial", 30F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Avere"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Avere"].HeaderCell.Style.Font = new Font("Arial", 20F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Avere"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Avere"].DefaultCellStyle.Font = new Font("Arial", 30F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Descrizione"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Descrizione"].HeaderCell.Style.Font = new Font("Arial", 20F, FontStyle.Bold, GraphicsUnit.Pixel);
                dt.Columns["Descrizione"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dt.Columns["Descrizione"].DefaultCellStyle.Font = new Font("Arial", 15F, FontStyle.Bold, GraphicsUnit.Pixel);

                dt.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            }
        }
    }
}
