﻿namespace WindowsFormsApp1
{
    partial class Popup2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.Data = new System.Windows.Forms.RadioButton();
            this.Dare = new System.Windows.Forms.RadioButton();
            this.Avere = new System.Windows.Forms.RadioButton();
            this.Descrizione = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.BackColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Font = new System.Drawing.Font("Cambria", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(-4, -1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(440, 49);
            this.label5.TabIndex = 9;
            this.label5.Text = "CERCA";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Data
            // 
            this.Data.AutoSize = true;
            this.Data.Checked = true;
            this.Data.Font = new System.Drawing.Font("Calibri", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Data.Location = new System.Drawing.Point(41, 51);
            this.Data.Name = "Data";
            this.Data.Size = new System.Drawing.Size(57, 21);
            this.Data.TabIndex = 10;
            this.Data.TabStop = true;
            this.Data.Text = "Data";
            this.Data.UseVisualStyleBackColor = true;
            this.Data.CheckedChanged += new System.EventHandler(this.Data_CheckedChanged);
            this.Data.Click += new System.EventHandler(this.Data_Click);
            // 
            // Dare
            // 
            this.Dare.AutoSize = true;
            this.Dare.Font = new System.Drawing.Font("Calibri", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dare.Location = new System.Drawing.Point(141, 51);
            this.Dare.Name = "Dare";
            this.Dare.Size = new System.Drawing.Size(57, 21);
            this.Dare.TabIndex = 11;
            this.Dare.Text = "Dare";
            this.Dare.UseVisualStyleBackColor = true;
            this.Dare.Click += new System.EventHandler(this.Data_Click);
            // 
            // Avere
            // 
            this.Avere.AutoSize = true;
            this.Avere.Font = new System.Drawing.Font("Calibri", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Avere.Location = new System.Drawing.Point(223, 51);
            this.Avere.Name = "Avere";
            this.Avere.Size = new System.Drawing.Size(63, 21);
            this.Avere.TabIndex = 12;
            this.Avere.Text = "Avere";
            this.Avere.UseVisualStyleBackColor = true;
            this.Avere.Click += new System.EventHandler(this.Data_Click);
            // 
            // Descrizione
            // 
            this.Descrizione.AutoSize = true;
            this.Descrizione.Font = new System.Drawing.Font("Calibri", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Descrizione.Location = new System.Drawing.Point(315, 51);
            this.Descrizione.Name = "Descrizione";
            this.Descrizione.Size = new System.Drawing.Size(97, 21);
            this.Descrizione.TabIndex = 13;
            this.Descrizione.Text = "Descrizione";
            this.Descrizione.UseVisualStyleBackColor = true;
            this.Descrizione.Click += new System.EventHandler(this.Data_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label6.Location = new System.Drawing.Point(258, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "label6";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Font = new System.Drawing.Font("Cambria", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(133, 122);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(153, 23);
            this.textBox1.TabIndex = 14;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Popup2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 191);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Descrizione);
            this.Controls.Add(this.Avere);
            this.Controls.Add(this.Dare);
            this.Controls.Add(this.Data);
            this.Controls.Add(this.label5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Popup2";
            this.Text = "Popup2";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Popup2_FormClosing);
            this.Load += new System.EventHandler(this.Popup2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton Data;
        private System.Windows.Forms.RadioButton Dare;
        private System.Windows.Forms.RadioButton Avere;
        private System.Windows.Forms.RadioButton Descrizione;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox1;
    }
}